<?php

namespace GpCore\Log;

use \Monolog\Logger;
	



class MonologProvider implements LogProvider {
	
	private $logger;

	public function __construct()
	{
		$this->logger = $logger = new Logger();
		$logger->pushHandler(new \Monolog\Handler\StreamHandler('/tmp/weater.log', Logger::DEBUG));
	}

	/**
     * Adds a log record at the LOG level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
	public function log($level, $message, array $context = [])
	{
		return $this->logger->log($message, $context);
	}

	 /**
     * Adds a log record at the DEBUG level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
	public function debug($message, array $context = [])
	{
		return $this->logger->debug($message, $context);	
	}

	/**
     * Adds a log record at the INFO level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
	public function info($message, array $context = [])
	{
		return $this->logger->info($message, $context);	
	}

	 /**
     * Adds a log record at the NOTICE level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
	public function notice($message, array $context = [])
	{
		return $this->logger->notice($message, $context);	
	}

	/**
     * Adds a log record at the WARNING level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
	public function warning($message, array $context = [])
	{
		return $this->logger->warning($message, $context);	
	}

	/**
     * Adds a log record at the ERROR level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
	public function error($message, array $context = [])
	{
		return $this->logger->error($message, $context);	
	}

	/**
     * Adds a log record at the CRITICAL level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
	public function critical($message, array $context = [])
	{
		return $this->logger->critical($message, $context);	
	}

	/**
     * Adds a log record at the ALERT level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
	public function alert($message, array $context = [])
	{
		return $this->logger->alert($message, $context);	
	}

	/**
     * Adds a log record at the EMERGENCY level.
     *
     * This method allows for compatibility with common interfaces.
     *
     * @param string $message The log message
     * @param array  $context The log context
     */
	public function emergency($message, array $context = [])
	{
		return $this->logger->emergency($message, $context);	
	}
}