<?php

namespace GpCore\Cache;

class RedisCacheProvider extends CacheProvider {

	private $client;

	const REDIS_NAMESPACE = 'gp::';
	
	public function __construct()
	{
		$this->client = new \Redis();
		if(!$this->client->connect('localhost')) {
			throw new \Exception('Could not connect to Redis');
		}
	}

	public function get($key, $default = null)
	{
		try {
			return $this->client->get(self::REDIS_NAMESPACE . $key) ?? $default;
		}
		catch(\RedisException $ex) {
			return $default;
		}
		
	}

	public function set($key, $value)
	{
		try {
			$this->client->set(self::REDIS_NAMESPACE . $key, $value);
		}
		catch(\RedisException $ex) {
			return $default;
		}
	}
}