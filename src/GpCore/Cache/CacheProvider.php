<?php

namespace GpCore\Cache;

abstract class CacheProvider {
	abstract function get($key, $default = null);
	abstract function set($key, $value);
}